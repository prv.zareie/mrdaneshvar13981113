package xo_game;

import java.util.Random;
import java.util.Scanner;

public class GameManager {
    static String previousChoice;
    private Random random = new Random();
    private Board board = new Board();
private Player player = Player.HUMAN;
    private static GameManager gameManager = new GameManager();
    private GameManager(){}
    public static GameManager getInstance()
    {
        return gameManager;
    }

    public void start() {

        Cell cell = new Cell();
        String input = "";
        boolean repeat = false;
        System.out.println("Welcome To XO;");
        while (board.hasEmptyCell()) {
            showMenu(cell,repeat);
            if(board.fill(cell))
            {
                repeat = false;
                GameStatus gameStatus = control();
                switch (gameStatus)
                {
                    case XWINNER:
                    System.out.println("X winnes");
                    board.print();
                    return;

                    case OWINNER:
                        System.out.println("O winnes");
                        board.print();
                        return;

                    case CONTINUE:
                        System.out.println("Continue");
                        break;
                }
                board.print();
            }
            else
            {
                repeat = true;
                System.out.println("The Cell Is Filled;Please Choose Another Cell");
            }
        }
        System.out.println("Game Over !");
    }

    private boolean checkInput(int input)
    {
        if(input < 1 || input > 3)
            return false;
        return true;
    }

    private void showMenu(Cell cell,boolean repeat)
    {
        String input = "",choice;
        Scanner scanner = new Scanner(System.in);
        int x = 0, y = 0;
        boolean isInputValid = false;

        if(repeat){
            player = (player.equals(Player.COMPUTER)?Player.HUMAN:Player.COMPUTER);
        }

        do {
            System.out.print("X : ");
            if(player == Player.HUMAN) {
                x = scanner.nextInt();
            }else{
                x = random.nextInt(3) + 1;
            }
            isInputValid = checkInput(x);
            if (!isInputValid) System.out.println("Wrong Input;Try Again");
        }while(!isInputValid);
        if(player.equals(Player.COMPUTER)) {
            System.out.println(x);
        }

        do {
            System.out.print("Y : ");
            if(player == Player.HUMAN) {
                y = scanner.nextInt();
            }else{
                y = random.nextInt(3) + 1;
            }
            isInputValid = checkInput(y);
            if (!isInputValid) System.out.println("Wrong Input;Try Again");
        }while(!isInputValid);
        if(player.equals(Player.COMPUTER)) {
            System.out.println(y);
        }

        do {
            System.out.print("x or o : ");
            if(player.equals(Player.HUMAN)) {
                choice = "x";
            }else{
                choice = "o";
            }
            System.out.println(choice);
            isInputValid = (choice.toLowerCase().equals("o") || choice.toLowerCase().equals("x"))? true:false;
            if (!isInputValid) {
                System.out.println("Wrong Input;Try Again");
                continue;
            }
            if(choice.equals(previousChoice) && !repeat) {
                System.out.println("Wrong Choice; Please Choose the Other Symbol");
                continue;
            }
            if(!repeat) {
                previousChoice = choice;
            }
            if(player.equals(Player.COMPUTER)) {
                player = Player.HUMAN;
            }
            else {
                player = Player.COMPUTER;
            }

            break;
        }while(true);

        cell.setX(x);
        cell.setY(y);
        cell.setChoice((choice.equals("x")? Choice.X:Choice.O));
    }

    private GameStatus control() {
        Choice choice;
        boolean isSomeoneWon = true;
        int i = 0;
        for (i = 0; i < board.getRowSize(); i++) {
            isSomeoneWon = true;
            for (int j = 0; j < board.getColumnSize() - 1; j++) {
                if (board.isCellFilled(i, j)) {
                    if (board.getCellChoice(i, j) != board.getCellChoice(i, j + 1)) {
                        isSomeoneWon = false;
                        break;
                    }
                } else {
                    isSomeoneWon = false;
                    break;
                }
            }
            if (isSomeoneWon) return (previousChoice.equals("x") ? GameStatus.XWINNER : GameStatus.OWINNER);
        }

        for (i = 0; i < board.getRowSize(); i++) {
            isSomeoneWon = true;
            for (int j = 0; j < board.getColumnSize() - 1; j++) {
                if (i == 1 && j == 1 && board.isCellFilled(i, j)) {
                    int a = 1;
                }
                if (board.isCellFilled(j, i)) {
                    if (board.getCellChoice(j, i) != board.getCellChoice(j + 1, i)) {
                        isSomeoneWon = false;
                        break;
                    }
                } else {
                    isSomeoneWon = false;
                    break;
                }
            }
            if (isSomeoneWon) return (previousChoice.equals("x") ? GameStatus.XWINNER : GameStatus.OWINNER);
        }

        isSomeoneWon = true;
        for (int j = 0; j < board.getColumnSize() - 1; j++) {
            if (board.isCellFilled(j, j)) {
                if (board.getCellChoice(j, j) != board.getCellChoice(j + 1, j + 1)) {
                    isSomeoneWon = false;
                    break;
                }
            } else {
                isSomeoneWon = false;
                break;
            }
        }
        if (isSomeoneWon) return (previousChoice.equals("x") ? GameStatus.XWINNER : GameStatus.OWINNER);

        i = 0;
        isSomeoneWon = true;
        for (int j = board.getColumnSize() - 1; j > 0; j--, i++) {
            if (board.isCellFilled(j, i)) {
                if (board.getCellChoice(j, i) != board.getCellChoice(j - 1, i + 1)) {
                    isSomeoneWon = false;
                    break;
                }
            } else {
                isSomeoneWon = false;
                break;
            }
        }
        if (isSomeoneWon) return (previousChoice.equals("x") ? GameStatus.XWINNER : GameStatus.OWINNER);

        return GameStatus.CONTINUE;
    }
}

