package xo_game;

public class Cell {
    private int x;
    private int y;
    private Choice choice;
    private boolean isFilled;

    public Cell(int x, int y, Choice choice) {
        this.x = x;
        this.y = y;
        this.choice = choice;
    }

    public Cell(int x, int y) {
        this.x = x;
        this.y = y;
        this.choice = Choice.UNKNOWN;
    }

    public Cell(){}

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public Choice getChoice() {
        return choice;
    }

    public void setChoice(Choice choice) {
        this.choice = choice;
    }

    public boolean isFilled() {
        return isFilled;
    }

    public void setFilled(boolean filled) {
        isFilled = filled;
    }
}
