package xo_game;

public class Board {
    private Cell[][] gameBoard = new Cell[3][3];

    public Board() {
        Cell cell;
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                cell = new Cell(i + 1, j + 1);
                gameBoard[i][j] = cell;
            }
        }
    }

    public boolean fill(Cell cell) {
        if (!isCellFilled(cell)) {
            setCellChoice(cell);
            gameBoard[cell.getX() - 1][cell.getY() - 1].setFilled(true);
            return true;
        } else {
            return false;
        }
    }

    public boolean hasEmptyCell()
    {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                if(! isCellFilled(gameBoard[i][j]))
                 return true;
            }
        }
        return  false;
    }

    public boolean isCellFilled(Cell cell) {
        return gameBoard[cell.getX() - 1][cell.getY() - 1].isFilled();
    }

    public boolean isCellFilled(int x, int y) {
        return gameBoard[x][y].isFilled();
    }

    public Choice getCellChoice(Cell cell) {
        return gameBoard[cell.getX() - 1][cell.getY() - 1].getChoice();
    }

    public Choice getCellChoice(int x, int y) {
        return gameBoard[x][y].getChoice();
    }

    private boolean setCellChoice(Cell cell) {
        gameBoard[cell.getX() - 1][cell.getY() - 1].setChoice(cell.getChoice());
        return true;
    }

    public int getRowSize() {
        return gameBoard.length;
    }

    public int getColumnSize() {
        return gameBoard[0].length;
    }

    public void print() {
        for (int i = 0; i < getRowSize(); i++) {
            for (int j = 0; j < getColumnSize(); j++) {
                if(gameBoard[i][j].getChoice() == Choice.X)
                    System.out.print("x ");
                else if(gameBoard[i][j].getChoice() == Choice.O)
                    System.out.print("o ");
                else
                    System.out.print("- ");
            }
            System.out.println();
        }
    }
}
