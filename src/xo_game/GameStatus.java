package xo_game;

public enum GameStatus {
    XWINNER,
    OWINNER,
    CONTINUE,
    UNKNOWN;
}
