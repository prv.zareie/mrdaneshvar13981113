package linked_list;

public class MyLinkedList<T> {
    private Node head;
    private Node tail;
    private int count;

    public void add(T data){
        if(head == null){
            head = new Node(data);
            tail = head;
        }else {
            Node nextNode = new Node(data);
            this.tail.setNextNode(nextNode);
            tail = nextNode;
        }
        count++;
    }

    public int find(T data){
        Node currentNode = head;
        int index = -1;
        while(currentNode!= null) {
            if(currentNode.getData().equals( data)){
                return ++index;
            }else{
                currentNode = currentNode.getNextNode();
                index++;
            }
        }
        return index;
    }

    public void remove(T data) {
        Node currentNode = head;
        Node prevNode = head;
        while (currentNode != null) {
            if (currentNode.getData().equals(data)) {
                if(currentNode == head){
                    head = head.getNextNode();
                }else {
                    prevNode.setNextNode(currentNode.getNextNode());
                }
                return;
            } else {
                prevNode = currentNode;
                currentNode = currentNode.getNextNode();
            }
        }
    }

    public String print(){
        String output = "";
        if(head == null){
            return output;
        }
        Node currentNode = head;
        while(currentNode != null){
            output += (currentNode.getData() + ";");
            currentNode = currentNode.getNextNode();
        }
        return output;
    }
}
