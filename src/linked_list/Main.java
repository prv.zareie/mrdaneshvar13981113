package linked_list;


public class Main {
    public static void main(String[] args) {
        MyLinkedList<Integer> sList = new MyLinkedList<>();

        sList.add(1);
        sList.add(2);
        sList.add(3);
        sList.add(4);
        System.out.println(sList.print());

        sList.remove(3);
        System.out.println(sList.print());

        int index = sList.find(4);
        System.out.println(index);
    }
}
