package hashemi.accounting;

import com.sun.media.sound.InvalidDataException;

import java.util.*;

public class Accounting {
    SortedMap<Integer,Health> healthSortedMap = new TreeMap<>();
    Queue<Grocery> groceries = new LinkedList<>();
    Stack<HomeAppliance> homeAppliances = new Stack<>();
	public void addCost(Cost cost) throws InvalidDataException {

	    if(cost instanceof Health)
		{
            healthSortedMap.put(((Health) cost).getPriority(),(Health)cost);
            return;
		}
		if(cost instanceof Grocery){
            groceries.add((Grocery)cost);
            return;
        }
        if(cost instanceof HomeAppliance){
            homeAppliances.push((HomeAppliance)cost);
            return;
        }
        throw new InvalidDataException();
	}

	public Cost getCost() {
        Cost cost = null;
        if (healthSortedMap.size() > 0) {
            Health health = healthSortedMap.get(healthSortedMap.lastKey());
            healthSortedMap.remove(healthSortedMap.lastKey());
            cost = health;
        } else if (groceries.size() > 0) {
            cost = groceries.remove();
        } else if (homeAppliances.size() > 0) {
            cost = homeAppliances.pop();
        }
        return cost;
    }

	public static void main(String[] args) {
        try {
            Accounting acc = new Accounting();
            acc.addCost(new Grocery(50000, "meat"));
            acc.addCost(new Health(100000, 1, "CalMagZink capsule"));
            acc.addCost(new HomeAppliance(4000000, "TV"));
            acc.addCost(new Health(60000, 4, "yearly check up"));
            acc.addCost(new Grocery(4000, "pancake powder"));
            acc.addCost(new HomeAppliance(600000, "pot"));

            System.out.println(acc.getCost());
            System.out.println(acc.getCost());
            System.out.println(acc.getCost());
            System.out.println(acc.getCost());
            System.out.println(acc.getCost());
            System.out.println(acc.getCost());
            System.out.println(acc.getCost());
        } catch (Exception ex) {
        }
    }

}
