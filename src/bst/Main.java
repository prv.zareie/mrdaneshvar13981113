package bst;

import java.util.ArrayList;
import java.util.LinkedList;
import static java.lang.System.out;

public class Main {
    public static void main(String[] args) {
        Tree tree = new Tree();
        tree.add(5, tree.getNode());
        tree.add(6, tree.getNode());
        tree.add(2, tree.getNode());
        tree.add(1, tree.getNode());
        tree.add(5, tree.getNode());
        tree.add(3, tree.getNode());
        tree.add(9, tree.getNode());
        tree.add(7, tree.getNode());
        tree.add(10, tree.getNode());
        tree.add(9, tree.getNode());
        tree.add(8, tree.getNode());

        printLevelOrder(tree.getNode());
    }

    //az internet dl kardam
    static void printLevelOrder(Node root) {
        LinkedList<QItem> queue = new LinkedList<>();
        ArrayList<Node> level = new ArrayList<>();
        int depth = height(root);
        queue.add(new QItem(root, depth));

        for (; ; ) {
            QItem curr = queue.poll();

            if (curr.depth < depth) {
                depth = curr.depth;

                for (int i = (int) Math.pow(2, depth) - 1; i > 0; i--) {
                    out.print(" ");
                }

                for (Node n : level) {
                    out.print(n == null ? " " : n.data);

                    for (int i = (int) Math.pow(2, depth + 1); i > 1; i--) {
                        out.print(" ");
                    }
                }

                out.println();
                level.clear();

                if (curr.depth <= 0) {
                    break;
                }
            }

            level.add(curr.node);

            if (curr.node == null) {
                queue.add(new QItem(null, depth - 1));
                queue.add(new QItem(null, depth - 1));
            } else {
                queue.add(new QItem(curr.node.leftNode, depth - 1));
                queue.add(new QItem(curr.node.rightNode, depth - 1));
            }
        }
    }

    //az internet dl kardam
    static int height(Node root) {
        return root == null ? 0 : 1 + Math.max(
                height(root.leftNode), height(root.rightNode)
        );
    }
}

//az internet dl kardam
    class QItem {
        Node node;
        int depth;

        public QItem(Node node, int depth) {
            this.node = node;
            this.depth = depth;
        }
    }


