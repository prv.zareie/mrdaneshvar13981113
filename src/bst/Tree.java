package bst;

public class Tree {
    private Node node;

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public void add(Integer data, Node tNode){
        if(node == null){node= new Node(data);return;}
        if(data > tNode.getData()){
            if(tNode.getRightNode() == null){
                tNode.setRightNode(new Node(data));
                return;
            }else {
                add(data, tNode.rightNode);
            }
        }else if(data < tNode.getData()){
            if(tNode.getLeftNode() == null){
                tNode.setLeftNode(new Node(data));
                return;
            }else {
                add(data, tNode.leftNode);
            }
        }else {
            return;
        }
    }
}
