package stack_queue;

import java.util.ArrayList;
import java.util.List;

public class Stack<T> {
    private List<T> stackList;
    public Stack()
    {
        stackList = new ArrayList<>();
    }

    public T push(T item)
    {
        stackList.add(item);
        return item;
    }

    public T pop() {
        if (empty()) {
            return null;
        }
        T item = peek();
        if (!item.equals(null)) {
            stackList.remove(item);
        }
        return item;
    }

    public boolean empty()
    {
        if(stackList.size() == 0){
            return true;
        }
        return false;
    }

    public T peek()
    {
        if (empty()) {
            return null;
        }
        T item = stackList.get(stackList.size() -1);
        return item;
    }

    @Override
    public String toString() {
        T[] array = (T[])stackList.toArray();
        String output = "";
        for (int i = array.length-1;i>=0;i--) {
            output += (array[i] + ";");
        }
        return output;
    }
}
