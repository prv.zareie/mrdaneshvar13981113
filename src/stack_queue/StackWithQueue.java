package stack_queue;

import java.util.LinkedList;
import java.util.Queue;

public class StackWithQueue<T> {
    private Queue<T> tQueue = new LinkedList<>();
    private Queue<T> tQueueTemp = new LinkedList<>();

    public T pop(T element) {
        if(empty()){
            return null;
        }
        return tQueueTemp.remove();
    }

    public T peek(T element) {
        if(empty()){
            return null;
        }
        return tQueueTemp.peek();
    }

    public T push(T element) {
        while(!tQueue.isEmpty()){
            tQueueTemp.add(tQueue.remove());
        }
         tQueue.add(element);
        while(!tQueueTemp.isEmpty()){
            tQueue.add(tQueueTemp.remove());
        }
        return element;
    }

    public boolean empty()
    {
        if(tQueueTemp.size() == 0){
            return true;
        }
        return false;
    }

    @Override
    public String toString() {
        T[] array = (T[])tQueue.toArray();
        String output = "";
        for (int i = 0;i<array.length;i++) {
            output += (array[i] + ";");
        }
        return output;
    }
}
