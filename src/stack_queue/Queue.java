package stack_queue;

import java.util.ArrayList;
import java.util.List;

public class Queue<T> {
    private List<T> list;
    private int firstIndex;
    private int lastIndex;
    public Queue()
    {
        list = new ArrayList<>();
    }

    public boolean add(T element) {
        list.add(element);
        lastIndex++;
        return true;
    }

    public boolean offer(T element){
        try {
            list.add(element);
            lastIndex++;
            return true;
        }
        catch (Exception ex)
        {
            return false;
        }
    }

    public T peek(){
        if(firstIndex > lastIndex)
        {
            return null;
        }
        return list.get(firstIndex);
    }

    public T element()throws Exception{
        if(firstIndex > lastIndex)
        {
            throw new Exception();
        }
        return list.get(firstIndex);
    }

    public T remove()throws Exception{
        if(firstIndex > lastIndex)
        {
           throw new Exception();
        }
        return list.get(firstIndex++);
    }

    public T poll(){
        if(firstIndex > lastIndex)
        {
            return null;
        }
        return list.get(firstIndex++);
    }

    @Override
    public String toString() {
        T[] array = (T[]) list.toArray();
        String output = "";
        for (int i = 0; i < list.size(); i++) {
            output += (array[i] + ";");
        }
        return output;
    }
}
