package stack_queue;

public class Main {
    public static void main(String[] args) {
        Stack<Integer> stack = new Stack<>();
        stack.push(1);
        stack.push(3);
        stack.push(5);
        System.out.println(stack.toString());

        StackWithQueue<Integer> stackWithQueue = new StackWithQueue<>();
        stackWithQueue.push(1);
        stackWithQueue.push(3);
        stackWithQueue.push(5);
        System.out.println(stackWithQueue.toString());

        Queue<Integer> queue = new Queue<>();
        queue.add(1);
        queue.add(3);
        queue.add(5);
        System.out.println(queue.toString());

        QueueWithStack<Integer> queueWithStack = new QueueWithStack<>();
        queueWithStack.add(1);
        queueWithStack.add(3);
        queueWithStack.add(5);
        System.out.println(queueWithStack.toString());
    }
}
