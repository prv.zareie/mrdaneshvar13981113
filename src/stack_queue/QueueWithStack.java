package stack_queue;

import java.util.Stack;

public class QueueWithStack<T> {
    private Stack<T> tStack = new Stack<>();
    private Stack<T> tStackTemp = new Stack<>();

    public boolean add(T element) {
        while(!tStack.empty()){
            tStackTemp.push(tStack.pop());
        }
        tStack.push(element);
        while(!tStackTemp.empty()){
            tStack.push(tStackTemp.pop());
        }
        return true;
    }

    public T peek(){
        if(!tStack.empty())
        {
            return tStack.peek();
        }
        return null;
    }

    public T pop() {
        if (! tStack.empty()) {
            return tStack.pop();
        }
        return null;
    }

    @Override
    public String toString() {
        String output = "";
        T[] array = (T[])tStack.toArray();
        for (int i = array.length-1;i>=0;i--) {
            output += (array[i] + ";");
        }
        return output;
    }

}
